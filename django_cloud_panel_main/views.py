from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_template_limitless.django_template_limitless.views import *


def get_nav(request):
    nav = list()

    nav.append(
        MenuGroup(
            menu_name=_("Directory- / Identity Services"),
            menu_icon="user",
            menu_items=(
                MenuLink(
                    link_name=_("Realms"),
                    link_url="/directory/realms",
                ),
                MenuLink(
                    link_name=_("Users"),
                    link_url="/directory/users",
                ),
                MenuLink(
                    link_name=_("Groups"),
                    link_url="/directory/groups",
                ),
                MenuLink(
                    link_name=_("Computers"),
                    link_url="/directory/computers",
                ),
                MenuLink(
                    link_name=_("Contacts"),
                    link_url="/directory/contacts",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Domains / DNS"),
            menu_icon="sphere",
            menu_items=(
                MenuLink(
                    link_name=_("Domain Overview"),
                    link_url="/whstack/domains",
                ),
                MenuLink(
                    link_name=_("Register Domain"),
                    link_url="/whstack/domains/register",
                ),
                MenuLink(
                    link_name=_("Transfer Domain"),
                    link_url="/whstack/domains/transfer",
                ),
                MenuLink(
                    link_name=_("DNS-Zone Overview"),
                    link_url="/whstack/zones",
                ),
                MenuLink(
                    link_name=_("DNS-Zone Templates"),
                    link_url="/whstack/template-zones",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("WWW-Publishing"),
            menu_icon="earth",
            menu_items=(
                MenuLink(
                    link_name=_("Websites"),
                    link_url="/whstack/websites",
                ),
                MenuLink(
                    link_name=_("FTP-Users"),
                    link_url="/whstack/ftp-users",
                ),
                MenuLink(
                    link_name=_("Databases - MariaDB"),
                    link_url="/whstack/db-mariadbs",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Unified Communications"),
            menu_icon="bubbles3",
            menu_items=(
                MenuLink(
                    link_name=_("Microsoft Exchange"),
                    link_url="/ucstack/exchange",
                ),
                MenuLink(
                    link_name=_("User Mailboxes"),
                    link_url="/ucstack/user-mailboxes",
                ),
                MenuLink(
                    link_name=_("Resource Mailboxes"),
                    link_url="/ucstack/resource-mailboxes",
                ),
                MenuLink(
                    link_name=_("Team Chat"),
                    link_url="/ucstack/team",
                ),
                MenuLink(
                    link_name=_("Meet Conferences"),
                    link_url="/ucstack/meet",
                ),
                MenuLink(
                    link_name=_("Filesharing"),
                    link_url="/ucstack/filesharing",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("E-Mail"),
            menu_icon="envelop2",
            menu_items=(
                MenuLink(
                    link_name=_("Archiving"),
                    link_url="/ucstack/archiving",
                ),
                MenuLink(
                    link_name=_("Classic (IMAP, POP3, SMTP)"),
                    link_url="/whstack/classic-emails",
                ),
                MenuLink(
                    link_name=_("Newsletter / Mass Mailing"),
                    link_url="/whstack/newsletter-emails",
                ),
                MenuLink(
                    link_name=_("Forwards"),
                    link_url="/whstack/email-forwards",
                ),
                MenuLink(
                    link_name=_("Spam Protection"),
                    link_url="/whstack/spam-protection",
                ),
                MenuLink(
                    link_name=_("Whitelist"),
                    link_url="/whstack/email-whitelist",
                ),
                MenuLink(
                    link_name=_("Blacklist"),
                    link_url="/whstack/email-blacklist",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Compute Cloud"),
            menu_icon="cloud",
            menu_items=(
                MenuLink(
                    link_name=_("Kubernetes"),
                    link_url="/cloudstack/kubernetes",
                ),
                MenuLink(
                    link_name=_("Virtual Machines"),
                    link_url="/cloudstack/virtual-machines",
                ),
                MenuLink(
                    link_name=_("Virtual Networks"),
                    link_url="/datacenter-manager/virtual-networks",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Network Management"),
            menu_icon="lan",
            menu_items=(
                MenuLink(
                    link_name=_("Routers"),
                    link_url="/datacenter-manager/routers",
                ),
                MenuLink(
                    link_name=_("IP-Subnets"),
                    link_url="/datacenter-manager/ipsubnets",
                ),
                MenuLink(
                    link_name=_("VLANs"),
                    link_url="/datacenter-manager/vlans",
                ),
                MenuLink(
                    link_name=_("WLAN Nodes"),
                    link_url="/wifistack/nodes",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("IT-Security"),
            menu_icon="shield-check",
            menu_items=(
                MenuLink(
                    link_name=_("Sophos"),
                    link_url="/security-backend/sophos",
                ),
                MenuLink(
                    link_name=_("ESET"),
                    link_url="/security-backend/eset",
                ),
                MenuLink(
                    link_name=_("Kaspersky"),
                    link_url="/security-backend/kaspersky",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Building Automation"),
            menu_icon="loop",
            menu_items=(
                MenuLink(
                    link_name=_("IoT Devices"),
                    link_url="/iotstack/devices",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Config-Management DB"),
            menu_icon="cogs",
            menu_items=(
                MenuLink(
                    link_name=_("Hosts"),
                    link_url="/cdstack/hosts",
                ),
                MenuLink(
                    link_name=_("Instances"),
                    link_url="/cdstack/instances",
                ),
                MenuLink(
                    link_name=_("Setting-Groups & Cluster"),
                    link_url="/cdstack/groups",
                ),
                MenuLink(
                    link_name=_("Meta Data"),
                    link_url="/cdstack/meta-datas",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("DevOps"),
            menu_icon="code",
            menu_items=(
                MenuLink(
                    link_name=_("Hosted GIT"),
                    link_url="/devops-backend/hosted-git",
                ),
                MenuLink(
                    link_name=_("Continuous Integration (CI)"),
                    link_url="/devops-backend/continuous-integration",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Public Key Infrastructure"),
            menu_icon="key",
            menu_items=(
                MenuLink(
                    link_name=_("Certificate Authorities"),
                    link_url="/pki-manager/cas",
                ),
                MenuLink(
                    link_name=_("Certificates"),
                    link_url="/pki-manager/certs",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Internet Access"),
            menu_icon="share3",
            menu_items=(
                MenuLink(
                    link_name=_("Locations"),
                    link_url="/ispstack/cpes",
                ),
                MenuLink(
                    link_name=_("DynDNS Hostnames"),
                    link_url="/dyndns/hostnames",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Telephony"),
            menu_icon="phone",
            menu_items=(
                MenuLink(
                    link_name=_("Trunks"),
                    link_url="/voipstack/trunks",
                ),
                MenuLink(
                    link_name=_("Numbers"),
                    link_url="/voipstack/numbers",
                ),
                MenuLink(
                    link_name=_("Routing Rules"),
                    link_url="/voipstack/routings",
                ),
                MenuLink(
                    link_name=_("CloudPBX"),
                    link_url="/voipstack/cloudpbx",
                ),
                MenuLink(
                    link_name=_("E-Fax (Fax/E-Mail)"),
                    link_url="/voipstack/e-faxs",
                ),
                MenuLink(
                    link_name=_("Blacklist"),
                    link_url="/voipstack/blacklist",
                ),
                MenuLink(
                    link_name=_("SMS Gateway"),
                    link_url="/smsgw/config",
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Value Added Reseller"),
            menu_icon="cart",
            menu_items=(
                MenuLink(
                    link_name=_("Customers"),
                    link_url="/mdat/customers",
                ),
            ),
        )
    )

    return nav


@login_required
def access_denied(request):
    template = loader.get_template("django_cloud_panel_main/access_denied.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Cloud Panel")
    template_opts["content_title_sub"] = _("Access Denied")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))
