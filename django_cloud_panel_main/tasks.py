from celery import shared_task
from django.core.management import call_command


@shared_task(name="django_cloud_panel_ldap_sync")
def django_cloud_panel_ldap_sync():
    call_command("ldap_sync_users")

    return
