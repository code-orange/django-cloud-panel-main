from django.core.management.base import BaseCommand

from django_cloud_panel_main.django_cloud_panel_main.tasks import (
    django_cloud_panel_ldap_sync,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        django_cloud_panel_ldap_sync()
        return
