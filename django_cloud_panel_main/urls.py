from django.urls import path

from . import views

urlpatterns = [
    path("access-denied", views.access_denied, name="main-access-denied"),
]
